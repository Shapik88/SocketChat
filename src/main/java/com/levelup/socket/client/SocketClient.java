package com.levelup.socket.client;


        import java.io.IOException;
        import java.io.PrintWriter;
        import java.net.Socket;
        import java.net.UnknownHostException;
        import java.util.Random;
        import java.util.Scanner;

        import static com.levelup.utils.SockeUtils.sendMessage;

public class SocketClient {
    public final int PORT = 8080;
    public final String IpAdress = "192.168.0.103";
    Scanner scanner = new Scanner(System.in);

    public void connect() {
        try (Socket socket = new Socket(IpAdress, PORT)) {

            PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
            System.out.print("Enter your nickname: ");
            String clientName = scanner.nextLine();
            while (true) {
                String message = scanner.nextLine();
                sendMessage("Client " + clientName + ": " + message, printWriter);
                Thread.sleep(3000);
            }


        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
