package com.levelup.socket;

import com.levelup.socket.client.SocketClient;

import java.io.IOException;

public class MainClient {


    public static void main(String[] args) throws IOException, InterruptedException {
        SocketClient socketClient = new SocketClient();
        socketClient.connect();
    }
}
