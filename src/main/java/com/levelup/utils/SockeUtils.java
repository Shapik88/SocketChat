package com.levelup.utils;

//import java.io.BufferedReader;
//import java.io.IOException;
import java.io.PrintWriter;

public class SockeUtils {

//    public static void waitForMessage(String s, BufferedReader bufferedReader) throws IOException {
//        String line;
//        while ((line = bufferedReader.readLine()) != null) {
//            System.out.println("Message received: " + line);
//            if (line.equals(s)) {
//                break;
//            }
//        }
//    }


    public static void sendMessage(String s, PrintWriter printWriter) {
        printWriter.println(s);
        printWriter.flush();
    }
}
